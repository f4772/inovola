class ReserveTypeModel {
  int? id;
  String? name;
  int? count;
  int? price;

  ReserveTypeModel({
    this.id,
    this.name,
    this.count,
    this.price,
  });

  factory ReserveTypeModel.fromJson(Map<String, dynamic> json) =>
      ReserveTypeModel(
        id: json["id"],
        name: json["name"],
        count: json["count"],
        price: json["price"],
      );
}
