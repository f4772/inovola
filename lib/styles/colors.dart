import 'package:flutter/material.dart';

class AppColors{
  static const grey = Color(0XFF9ea3b8);
  static const lightGrey = Color(0XFFb9bed1);
  static const purple = Color(0XFF8c3ca2);
  static const white = Color(0XFFFFFFFF);
}