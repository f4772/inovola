import 'package:dio/dio.dart';

class DioHelper {
  static late Dio dio;

  static init() {
    dio = Dio(
      BaseOptions(
          baseUrl: "https://run.mocky.io/v3/",
          headers: {"Content-Type": "application/json"}),
    );
  }

  static Future<Response> getData({
    required String path,
  }) async {
    return await dio.get(path);
  }
}
