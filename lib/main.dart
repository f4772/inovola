import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:inovola_task/UI/course_screen.dart';
import 'package:inovola_task/cubit/app_cubit.dart';
import 'package:inovola_task/cubit/app_states.dart';
import 'package:inovola_task/network_helper.dart';

void main() async {
  await DioHelper.init();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (BuildContext context) => AppCubit()..fetchAppData(),
        ),
      ],
      child: BlocConsumer<AppCubit, AppStates>(
          listener: (context, state) {},
          builder: (BuildContext context, state) {
            return const MaterialApp(
              debugShowCheckedModeBanner: false,
              home: Directionality(
                child: CourseScreen(),
                textDirection: TextDirection.rtl,
              ),
            );
          }),
    );
  }
}
