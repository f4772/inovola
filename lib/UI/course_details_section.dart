import 'package:flutter/material.dart';
import 'package:inovola_task/models/course.dart';
import 'package:inovola_task/styles/colors.dart';

class CourseDetailsSection extends StatelessWidget {
  const CourseDetailsSection({ Key? key,this.course}) : super(key: key);
  final CourseModel? course;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Text(
            'عن الدورة',
            style: TextStyle(
              color: AppColors.lightGrey,
              fontWeight: FontWeight.bold,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 5.0),
            child: Text(
              course?.occasionDetail ?? '',
              style: const TextStyle(
                color: AppColors.lightGrey,
              ),
            ),
          )
        ],
      ),
    );
  }
}