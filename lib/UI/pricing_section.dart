import 'package:flutter/material.dart';
import 'package:inovola_task/models/course.dart';
import 'package:inovola_task/models/reserve_type.dart';
import 'package:inovola_task/styles/colors.dart';

class PricingSection extends StatelessWidget {
  const PricingSection({ Key? key,this.course }) : super(key: key);
  final CourseModel? course;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Text(
            "تكلفة الدورة",
            style: TextStyle(
              color: AppColors.lightGrey,
              fontWeight: FontWeight.bold,
            ),
          ),
          // _buildPricingRow(course?.reservTypes?[0])
          for (ReserveTypeModel r in course?.reservTypes ?? [])
            _buildPricingRow(r)
        ],
      ),
    );
  }


  Widget _buildPricingRow(ReserveTypeModel? reserve) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 5.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(reserve?.name ?? '',
              style: const TextStyle(color: AppColors.lightGrey)),
          Text(reserve?.price.toString() ?? '',
              style: const TextStyle(color: AppColors.lightGrey)),
        ],
      ),
    );
  }
}