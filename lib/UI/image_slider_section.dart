import 'package:carousel_indicator/carousel_indicator.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:inovola_task/cubit/app_cubit.dart';
import 'package:inovola_task/styles/colors.dart';

class ImageSliderSection extends StatelessWidget {
  ImageSliderSection({Key? key, required this.cubit}) : super(key: key);
  final AppCubit cubit;
  final List<String> images = [
    "https://img.freepik.com/free-photo/cup-coffee-coffee-beans_164008-356.jpg",
    "https://img.freepik.com/free-photo/hands-digital-universe-background_53876-97068.jpg",
    "https://img.freepik.com/free-photo/futuristic-fantasy-night-landscape-with-abstract-islands-night-sky-with-space-galaxies_129911-2852.jpg",
  ];

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        CarouselSlider(
          items: images
              .map((image) => Image(
                    image: NetworkImage(image),
                    width: MediaQuery.of(context).size.width,
                    fit: BoxFit.cover,
                  ))
              .toList(),
          options: CarouselOptions(
              height: 250,
              initialPage: 0,
              enableInfiniteScroll: true,
              reverse: false,
              autoPlay: true,
              autoPlayAnimationDuration: const Duration(seconds: 1),
              autoPlayCurve: Curves.fastOutSlowIn,
              scrollDirection: Axis.horizontal,
              viewportFraction: 1,
              onPageChanged: (index, _) {
                cubit.updateImageIndex(index);
              }),
        ),
        Positioned(
          top: 20,
          left: 5,
          right: 5,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              IconButton(
                onPressed: () {},
                icon: const Icon(Icons.arrow_back_ios, color: AppColors.white),
              ),
              Row(
                children: [
                  IconButton(
                    onPressed: () {},
                    icon: const Icon(Icons.share_outlined,
                        color: AppColors.white, size: 24),
                  ),
                  IconButton(
                    onPressed: () {},
                    icon: const Icon(Icons.star_border,
                        color: AppColors.white, size: 24),
                  ),
                ],
              ),
            ],
          ),
        ),
        Positioned(
          left: 20,
          bottom: 20,
          child: CarouselIndicator(
            width: 10,
            height: 10,
            animationDuration: 150,
            cornerRadius: 50,
            count: images.length,
            index: cubit.imageIndex,
          ),
        ),
      ],
    );
  }
}
