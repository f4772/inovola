import 'package:flutter/material.dart';
import 'package:inovola_task/models/course.dart';
import 'package:inovola_task/styles/colors.dart';

class TrainerDataSection extends StatelessWidget {
  const TrainerDataSection({Key? key, this.course}) : super(key: key);
  final CourseModel? course;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              const Padding(
                padding: EdgeInsetsDirectional.only(end: 8.0),
                child: CircleAvatar(
                  radius: 15,
                  backgroundImage: NetworkImage(
                      'https://img.freepik.com/free-photo/confident-young-businessman-suit-standing-with-arms-folded_171337-18599.jpg'),
                ),
              ),
              Text(
                course?.trainerName ?? '',
                style: const TextStyle(
                  color: AppColors.lightGrey,
                  fontWeight: FontWeight.bold,
                ),
              )
            ],
          ),
          Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: Text(
              course?.trainerInfo ?? '',
              style: const TextStyle(
                color: AppColors.lightGrey,
              ),
            ),
          )
        ],
      ),
    );
  }
}
