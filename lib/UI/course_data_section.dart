import 'package:flutter/material.dart';
import 'package:inovola_task/models/course.dart';
import 'package:inovola_task/styles/colors.dart';
import 'package:intl/intl.dart';

class CourseDataSection extends StatelessWidget {
  const CourseDataSection({Key? key, this.course}) : super(key: key);
  final CourseModel? course;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "# ${course?.interest}",
            style: const TextStyle(
              color: AppColors.lightGrey,
              fontSize: 16,
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0),
            child: Text(
              course?.title ?? '',
              style: const TextStyle(
                color: AppColors.grey,
                fontSize: 18,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Row(
            children: [
              const Icon(Icons.date_range_outlined, color: AppColors.lightGrey),
              Padding(
                padding: const EdgeInsetsDirectional.only(start: 5.0),
                child: Text(
                  DateFormat('EEE, d MMM, hh:mm aaa', 'ar').format(
                    course?.date ?? DateTime.now(),
                  ),
                  style: const TextStyle(color: AppColors.lightGrey),
                ),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: Row(
              children: [
                const Icon(Icons.push_pin_outlined, color: AppColors.lightGrey),
                Padding(
                  padding: const EdgeInsetsDirectional.only(start: 5.0),
                  child: Text(
                    course?.address ?? '',
                    style: const TextStyle(color: AppColors.lightGrey),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
