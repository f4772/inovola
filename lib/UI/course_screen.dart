import 'package:buildcondition/buildcondition.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:inovola_task/UI/course_data_section.dart';
import 'package:inovola_task/UI/course_details_section.dart';
import 'package:inovola_task/UI/image_slider_section.dart';
import 'package:inovola_task/UI/pricing_section.dart';
import 'package:inovola_task/UI/trainer_data_section.dart';
import 'package:inovola_task/cubit/app_cubit.dart';
import 'package:inovola_task/cubit/app_states.dart';
import 'package:inovola_task/models/course.dart';
import 'package:inovola_task/styles/colors.dart';
import 'package:intl/date_symbol_data_local.dart';

class CourseScreen extends StatelessWidget {
  const CourseScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<AppCubit, AppStates>(
      listener: (context, state) {},
      builder: (context, state) {
        initializeDateFormatting('ar');
        AppCubit cubit = AppCubit.get(context);
        CourseModel? course = cubit.course;
        return Scaffold(
          bottomNavigationBar: _buildButton(),
          body: BuildCondition(
            condition: course != null,
            fallback: (context) =>
                const Center(child: CircularProgressIndicator()),
            builder: (context) => SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  ImageSliderSection(cubit: cubit,),
                  CourseDataSection(course: course),
                  const Divider(),
                  TrainerDataSection(course: course),
                  const Divider(),
                  CourseDetailsSection(course: course),
                  const Divider(),
                  PricingSection(course: course),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  Widget _buildButton() {
    return Padding(
      padding: const EdgeInsets.only(top: 10.0),
      child: InkWell(
        onTap: () {},
        child: Container(
            height: 60,
            decoration: const BoxDecoration(color: AppColors.purple),
            child: const Center(
              child: Text(
                "قم بالحجز الان",
                style: TextStyle(
                    color: AppColors.white, fontWeight: FontWeight.w800),
              ),
            )),
      ),
    );
  }
}
