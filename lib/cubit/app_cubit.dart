import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:inovola_task/cubit/app_states.dart';
import 'package:inovola_task/models/course.dart';
import 'package:inovola_task/network_helper.dart';

class AppCubit extends Cubit<AppStates> {
  AppCubit() : super(AppInitialState());
  static AppCubit get(context) => BlocProvider.of(context);

  CourseModel? course;
  void fetchAppData() {
    emit(FetchDataLoadingState());
    DioHelper.getData(path: "3a1ec9ff-6a95-43cf-8be7-f5daa2122a34")
        .then((value) {
      course = CourseModel.fromJson(value.data);
      emit(FetchDataSuccessState());
    }).catchError((onError) {
      debugPrint(onError.toString());
      emit(FetchDataErrorState(onError.toString()));
    });
  }

  int imageIndex = 0;
  void updateImageIndex(int index){
    imageIndex = index;
    emit(ImageIndexUpdated());
  }
}
