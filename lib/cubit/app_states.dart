abstract class AppStates {}

class AppInitialState extends AppStates {}

class FetchDataLoadingState extends AppStates{}

class FetchDataSuccessState extends AppStates{}

class FetchDataErrorState extends AppStates{
  String error;
  FetchDataErrorState(this.error);
}

class ImageIndexUpdated extends AppStates{}